package org.example.framework.server.http;

import org.example.framework.server.parser.QueryParser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.*;

class RequestTest {
    @Test
    void methodsShouldWorks() {
        String query = "text=java&type=ebook&type=course";

        Map<String, List<String>> expected = new LinkedHashMap<>();
        List<String> text = new ArrayList<>();
        text.add("java");
        List<String> type = new ArrayList<>();
        type.add("ebook");
        type.add("course");
        expected.put("text", text);
        expected.put("type", type);

        Request request = new Request();
        request.setQuery(query);
        QueryParser.parse(request);

        Assertions.assertEquals(Optional.of("ebook"), request.getQueryParam("type"));
        Assertions.assertEquals(Optional.empty(), request.getQueryParam("value"));

        Assertions.assertIterableEquals(type, request.getQueryParams("type"));

        Map<String, List<String>> actual = request.getAllQueryParams();

        Assertions.assertIterableEquals(expected.get("text"), actual.get("text"));
        Assertions.assertIterableEquals(expected.get("type"), actual.get("type"));
    }
}
