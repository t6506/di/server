package org.example.framework.server.router;

import lombok.RequiredArgsConstructor;
import org.example.framework.server.controller.method.handler.ReturnValueHandler;
import org.example.framework.server.exception.RequestProcessException;
import org.example.framework.server.handler.Handler;
import org.example.framework.server.http.Request;
import org.example.framework.server.controller.method.resolver.ArgumentResolver;
import org.example.framework.server.http.Response;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class MethodRouteAdapter implements Handler {
    private final MethodRoute route;
    private final List<ArgumentResolver> argumentResolvers;
    private final List<ReturnValueHandler> returnValueHandlers;


    @Override
    public void handle(final Request request, final Response response) {
        final Method method = route.getBeanMethod();
        final Object bean = route.getBean();
        try {
            List<Object> args = new ArrayList<>(method.getParameterCount());
            for (Parameter parameter : method.getParameters()) {
                for (ArgumentResolver argumentResolver : argumentResolvers) {
                    if (argumentResolver.supportParameter(parameter)) {
                        args.add(argumentResolver.resolveArgument(parameter, request));
                        break;
                    }
                }
            }

            final Object result = method.invoke(bean, args.toArray());

            for (ReturnValueHandler returnValueHandler : returnValueHandlers) {
                if (!returnValueHandler.supportReturnType(method)) {
                    continue;
                }
                returnValueHandler.handleReturnType(result, method, request, response);
            }

        } catch (Exception e) {
            throw new RequestProcessException(e);
        }
    }
}
