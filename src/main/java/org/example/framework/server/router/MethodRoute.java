package org.example.framework.server.router;

import lombok.Builder;
import lombok.Data;

import java.lang.reflect.Method;
import java.util.regex.Matcher;

@Builder
@Data
public class MethodRoute {
    private final Object bean;
    private final Method beanMethod;
    private final Matcher matcher;
}
