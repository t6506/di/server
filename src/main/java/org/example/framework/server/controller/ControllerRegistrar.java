package org.example.framework.server.controller;

import org.example.framework.server.annotation.Controller;
import org.example.framework.server.annotation.RequestMapping;
import org.example.framework.server.router.MethodRouter;
import org.example.framework.server.exception.InvalidControllerBeanException;
import org.example.framework.server.http.HttpMethod;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class ControllerRegistrar {

    public MethodRouter register(final List<?> beans) {
        MethodRouter methodRouter = new MethodRouter();
        for (final Object bean : beans) {
            register(bean, methodRouter);
        }
        return methodRouter;
    }
    private void register(final Object bean, final MethodRouter route) {
        Class<?> clazz = bean.getClass();
        if (!clazz.isAnnotationPresent(Controller.class)) {
            throw new InvalidControllerBeanException("annotation @Controller didn't present: " + clazz.getName());
        }
        final List<Method> beanMethods = Arrays.stream(clazz.getMethods())
                .filter(o -> o.isAnnotationPresent(RequestMapping.class))
                .collect(Collectors.toList());

        for (Method beanMethod : beanMethods) {
            final RequestMapping requestMapping = beanMethod.getAnnotation(RequestMapping.class);
            HttpMethod httpMethod = requestMapping.method();
            String path = requestMapping.path();
            Pattern pattern = Pattern.compile(path);
            route.register(httpMethod, pattern, bean, beanMethod);
        }
    }


}
