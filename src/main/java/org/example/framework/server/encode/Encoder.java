package org.example.framework.server.encode;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Slf4j
public class Encoder {
    public static void main(String[] args) {
        final PasswordEncoder encoder =new Argon2PasswordEncoder();
        final String encodePassword = encoder.encode("password");
        log.debug(encodePassword);
    }
}
