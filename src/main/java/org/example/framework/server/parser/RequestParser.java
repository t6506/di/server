package org.example.framework.server.parser;

import lombok.extern.slf4j.Slf4j;
import org.example.framework.server.exception.InvalidHeaderLineStructureException;
import org.example.framework.server.exception.InvalidRequestLineStructureException;
import org.example.framework.server.exception.InvalidRequestStructureException;
import org.example.framework.server.http.HttpHeader;
import org.example.framework.server.http.Request;
import org.example.framework.server.util.Bytes;


import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

@Slf4j
public class RequestParser {
    private static final byte[] CRLF = new byte[]{'\r', '\n'};
    private static final byte[] CRLF_CRLF = new byte[]{'\r', '\n', '\r', '\n'};

    public static void parse(final byte[] buffer, Request request) throws UnsupportedEncodingException {

        final int requestLineEndIndex = Bytes.indexOf(buffer, CRLF);
        final int headerStartIndex = requestLineEndIndex + CRLF.length;
        final int headersEndIndex = Bytes.indexOf(buffer, CRLF_CRLF, headerStartIndex);

        if (requestLineEndIndex == -1) {
            log.debug("request line end not found");
            throw new InvalidRequestLineStructureException();

        }
        if (headersEndIndex == -1) {
            log.debug("header end not found");
            throw new InvalidHeaderLineStructureException();
        }

        String requestLine = new String(buffer, 0, requestLineEndIndex, StandardCharsets.UTF_8);
        final byte[] headers = Arrays.copyOfRange(buffer, headerStartIndex, headersEndIndex + CRLF.length);
        parseRequestLine(requestLine, request);
        parseHeaders(headers, request);
        QueryParser.parse(request);
        log.debug("parsed request line and headers: \n{}", request);
    }

    private static void parseRequestLine(final String requestLine, Request request) throws UnsupportedEncodingException {
        String query = null;
        String[] splitRequestLine = requestLine.split("\\s+");
        if (splitRequestLine.length != 3) {
            log.debug("request line invalid structure");
            throw new InvalidRequestLineStructureException();

        }
        String[] pathAndQuery = URLDecoder.decode(splitRequestLine[1], StandardCharsets.UTF_8.name()).split("\\?", 2);
        if (pathAndQuery.length == 2) {
            query = pathAndQuery[1];
        }
        request.setMethod(splitRequestLine[0]);
        request.setPath(pathAndQuery[0]);
        request.setQuery(query);
        request.setHttpVersion(splitRequestLine[2]);
    }

    private static void parseHeaders(final byte[] bytes, final Request request) {
        Map<String, String> headers = new LinkedHashMap<>();
        int lastProcessedIndex = 0;
        while (lastProcessedIndex < bytes.length - CRLF.length) {
            final int currentHeaderEndIndex = Bytes.indexOf(bytes, CRLF, lastProcessedIndex);
            String currentHeader = new String(bytes, lastProcessedIndex, currentHeaderEndIndex - lastProcessedIndex, StandardCharsets.UTF_8);
            lastProcessedIndex = currentHeaderEndIndex + CRLF.length;
            String[] splitCurrentHeader = currentHeader.split(":\\s*", 2);
            if (splitCurrentHeader.length != 2) {
                log.debug("current header invalid structure");
                throw new InvalidRequestStructureException();
            }
            headers.put(splitCurrentHeader[0].toLowerCase(), splitCurrentHeader[1]);
            if (splitCurrentHeader[0].equalsIgnoreCase(HttpHeader.CONTENT_LENGTH.value())) {
                request.setContentLength(Integer.parseInt(splitCurrentHeader[1]));
            }
        }
        request.setHeaders(headers);
    }
}
