package org.example.framework.server.http;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Singular;
import lombok.extern.slf4j.Slf4j;
import org.example.framework.server.auth.SecurityContext;
import org.example.framework.server.controller.method.resolver.ArgumentResolver;
import org.example.framework.server.controller.method.handler.ReturnValueHandler;
import org.example.framework.server.router.MethodRouter;
import org.example.framework.server.handler.Handler;
import org.example.framework.server.middleware.Middleware;
import org.example.framework.server.parser.RequestParser;
import org.example.framework.server.exception.MarkNotSupportedException;
import org.example.framework.server.exception.MethodNotAllowedException;
import org.example.framework.server.exception.ResourceNotFoundException;
import org.example.framework.server.router.MethodRouteAdapter;
import org.example.framework.server.router.MethodRoute;
import org.example.framework.server.util.Bytes;

import javax.net.ServerSocketFactory;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@RequiredArgsConstructor
@AllArgsConstructor
@Builder

public class Server {
  private static final int MAX_REQUEST_LINE_AND_HEADERS_SIZE = 4096;
  private static final byte[] CRLF_CRLF = new byte[]{'\r', '\n', '\r', '\n'};
  private static final int MAX_CONTENT_LENGTH = 10 * 1024 * 1024;

  private final AtomicInteger workerCounter = new AtomicInteger();
  private final ExecutorService workers = Executors.newFixedThreadPool(64, runnable -> {
    final Thread worker = new Thread(runnable);
    worker.setName("worker-" + workerCounter.incrementAndGet());
    return worker;
  });
  private ServerSocket serverSocket;

  @Singular
  private final List<Middleware> middlewares;
  @Singular
  // String - path, String - method
  private final List<ArgumentResolver> argumentResolvers;

  private final List<ReturnValueHandler> returnValueHandlers;

  private final MethodRouter router;

  @Builder.Default
  private final Handler notFoundHandler = Handler::notFoundHandler;
  @Builder.Default
  private final Handler methodNotAllowed = Handler::methodNotAllowedHandler;
  @Builder.Default
  private final Handler internalServerErrorHandler = Handler::internalServerError;

  public void start(final int port) {
    new Thread(() -> {
      log.debug("new thread start for serve");
      try {
        serve(port);
      } catch (IOException e) {
        log.error("can't serve", e);
      }
    }).start();

  }

  private void serve(final int port) throws IOException {
    final ServerSocketFactory socketFactory = SSLServerSocketFactory.getDefault();
    serverSocket = socketFactory.createServerSocket(port);
    final SSLServerSocket sslServerSocket = (SSLServerSocket) serverSocket;
    sslServerSocket.setEnabledProtocols(new String[]{"TLSv1.2"});
    sslServerSocket.setWantClientAuth(true);
    log.info("server listen on {}", port);

    while (!serverSocket.isClosed()) {
      try {
        final Socket socket = serverSocket.accept();
        workers.submit(() -> handle(socket));
      }
      catch (SocketException e) {
        log.debug("socket closed");
      } catch (IOException e) {
        log.error("can't accept socket", e);
      }
    }
  }

  public void stop() throws IOException {
    threadPoolShutDown();
    serverSocket.close();
  }

  private void handle(Socket socket) {
    final byte[] buffer = new byte[MAX_REQUEST_LINE_AND_HEADERS_SIZE];

    try (final InputStream in = new BufferedInputStream(socket.getInputStream());
         final OutputStream out = socket.getOutputStream()) {
      log.debug("client connected: {}:{}", socket.getInetAddress(), socket.getPort());
      handleRequest(socket, buffer, in, out);
    }
    catch (Exception e) {
      log.error("can't handle request", e);
    }
    finally {
      SecurityContext.clear();
    }
  }

  private void handleRequest(Socket socket, byte[] buffer, InputStream in, OutputStream out) throws IOException {
    final Request request = new Request();
    final Response response = new Response(out);

    try {
      if (!in.markSupported()) {
        throw new MarkNotSupportedException();
      }
      in.mark(MAX_REQUEST_LINE_AND_HEADERS_SIZE);

      final int firstRead = in.read(buffer);
      log.debug("into buffer read {} bytes (first read)", firstRead);

      RequestParser.parse(buffer, request);

      final byte[] body = new byte[request.getContentLength()];
      in.reset();
      final int bodyStartIndex = Bytes.indexOf(buffer, CRLF_CRLF) + CRLF_CRLF.length;

      long skipRead = in.skip(bodyStartIndex);
      final int bodyRead = in.read(body);
      log.debug("into buffer read {} bytes (body read), skipped {} bytes", bodyRead, skipRead);
      request.setBody(body);

      for (final Middleware middleware : middlewares) {
        middleware.handle(socket, request);
      }

      final MethodRoute route = router.findController(request.getPath(), request.getMethod()).orElseThrow(ResourceNotFoundException::new);

      final MethodRouteAdapter adapter = new MethodRouteAdapter(route, argumentResolvers, returnValueHandlers);

      request.setPathMatcher(route.getMatcher());

      adapter.handle(request, response);

    }
    catch (MethodNotAllowedException e) {
      log.error("request method not allowed", e);
      methodNotAllowed.handle(request, response);
    } catch (ResourceNotFoundException e) {
      log.error("can't found request", e);
      notFoundHandler.handle(request, response);
    } catch (Exception e) {
      log.error("can't handle request", e);
      internalServerErrorHandler.handle(request, response);
    }
  }

  private void threadPoolShutDown() {
    try {
      workers.shutdown();
      if (workers.awaitTermination(10, TimeUnit.SECONDS)) {
        log.debug("thread pool shutdown, tasks completed");
        return;
      }
      log.debug("thread pool shutdown, timeout occurs");
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
    }
    finally {
      workers.shutdownNow();
    }
  }
}
