package org.example.framework.server.http;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;

@Slf4j
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Request {
  private String method;
  private String path;
  private Matcher pathMatcher;
  private String query;
  private Map<String, List<String>> queryParams = new LinkedHashMap<>();
  private String httpVersion;
  private Map<String, String> headers = new LinkedHashMap<>();
  private int contentLength;
  private byte[] body;

  public String getPathGroup(String name) {
    return pathMatcher.group(name);
  }

  public String getPathGroup(int index) {
    return pathMatcher.group(index);
  }

  public Optional<String> getQueryParam(String name) {
      return Optional.ofNullable(queryParams.get(name)).map(list -> list.get(0));

  }

  public List<String> getQueryParams(String name) {
    return queryParams.get(name);
  }

  public Map<String, List<String>> getAllQueryParams() {
    return queryParams;
  }

  public MediaType getContentType() {
    return Optional.ofNullable(headers.get(HttpHeader.CONTENT_TYPE.value().toLowerCase()))
            .map(MediaType::fromValue)
            .orElse(MediaType.ANY);
    }

  public MediaType getAccept() {
    return Optional.ofNullable(headers.get(HttpHeader.ACCEPT.value().toLowerCase()))
            .map(MediaType::fromValue)
            .orElse(MediaType.ANY);
  }
}
