package org.example.framework.server.http;

import lombok.RequiredArgsConstructor;
import org.example.framework.server.exception.ResponseWriteException;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

@RequiredArgsConstructor
public class Response {
    private final OutputStream out;


    public void writeResponse(final MediaType mediaType, final String body) {
        writeResponse(HttpStatus.S200, mediaType, body.getBytes(StandardCharsets.UTF_8));
    }

    public void writeResponse(final HttpStatus status, final MediaType mediaType, final String body) {
        writeResponse(status, mediaType, body.getBytes(StandardCharsets.UTF_8));
    }

    public void writeResponse(final HttpStatus status, final MediaType mediaType, final byte[] body) {
        try {
            out.write((
                    "HTTP/1.1 " + status.value() + "\r\n" +
                    "Content-Length: " + body.length + "\r\n" +
                    "Connection: close\r\n" +
                    "Content-Type: " + mediaType.value() + "\r\n" +
                    "\r\n"
            ).getBytes(StandardCharsets.UTF_8));
            out.write(body);
        } catch (IOException e) {
            throw new ResponseWriteException();
        }
    }


}
