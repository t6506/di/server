package org.example.framework.server.http;

public enum MediaType {
    APPLICATION_JSON("application/json"),
    TEXT_PLAIN("text/plain"),
    IMAGE_PNG("image/png"),
    ANY("*/*");

    private final String value;

    MediaType(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }

    public static MediaType fromValue(final String value) {
        for (MediaType mediaType : MediaType.values()) {
            if (mediaType.value.equals(value)) {
                return mediaType;
            }
        }
        throw new IllegalArgumentException(value);
    }


}
