package org.example.framework.server.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Maps {
  private Maps() {}

  public static <K, V> Map<K, V> of() {
    return Collections.emptyMap();
  }

  public static <K, V> Map<K, V> of(K key, V value) {
    return Collections.singletonMap(key, value);
  }

  public static <K, V> Map<K, V> of(K key1, V value1,
                                    K key2, V value2) {
    final Map<K, V> map = new HashMap<>(of(key1, value1));
    map.put(key2, value2);
    return map;
  }

  public static <K, V> Map<K, V> of(K key1, V value1,
                                    K key2, V value2,
                                    K key3, V value3) {
    final Map<K, V> map = new HashMap<>(of(key1, value1, key2, value2));
    map.put(key3, value3);
    return Collections.unmodifiableMap(map);
  }

  public static <K, V> Map<K, V> of(K key1, V value1,
                                    K key2, V value2,
                                    K key3, V value3,
                                    K key4, V value4) {
    final Map<K, V> map = new HashMap<>(of(key1, value1, key2, value2, key3, value3));
    map.put(key4, value4);
    return Collections.unmodifiableMap(map);
  }

  public static <K, V> Map<K, V> of(K key1, V value1,
                                    K key2, V value2,
                                    K key3, V value3,
                                    K key4, V value4,
                                    K key5, V value5) {
    final Map<K, V> map = new HashMap<>(of(key1, value1, key2, value2, key3, value3, key4, value4));
    map.put(key5, value5);
    return Collections.unmodifiableMap(map);
  }

  public static <K, V> Map<K, V> of(K key1, V value1,
                                    K key2, V value2,
                                    K key3, V value3,
                                    K key4, V value4,
                                    K key5, V value5,
                                    K key6, V value6) {
    final Map<K, V> map = new HashMap<>(of(key1, value1, key2, value2, key3, value3, key4, value4, key5, value5));
    map.put(key6, value6);
    return Collections.unmodifiableMap(map);
  }


}
