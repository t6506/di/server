package org.example.framework.server.annotation;

import org.example.framework.server.http.HttpMethod;
import org.example.framework.server.http.MediaType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface RequestMapping {
    HttpMethod method() default HttpMethod.GET;
    String path() default "";
    MediaType produces() default MediaType.APPLICATION_JSON;
}
