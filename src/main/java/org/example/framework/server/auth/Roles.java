package org.example.framework.server.auth;

public enum Roles {
    USER("User"),
    ANONYMOUS("Anonymous");

    private final String value;

    Roles(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}
