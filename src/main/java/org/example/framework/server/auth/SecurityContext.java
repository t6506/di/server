package org.example.framework.server.auth;

public class SecurityContext {
    private static final ThreadLocal<Principal> principalHolder = new ThreadLocal<>();

    private SecurityContext() {}

    public static void setPrincipal(final Principal principal) {
        principalHolder.set(principal);
    }

    public static Principal getPrincipal() {
        return principalHolder.get();
    }

    public static void clear() {
        principalHolder.remove();
    }
}
