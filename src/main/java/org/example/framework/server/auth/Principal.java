package org.example.framework.server.auth;

public interface Principal {
    String getName();
    Roles getRole();
}
