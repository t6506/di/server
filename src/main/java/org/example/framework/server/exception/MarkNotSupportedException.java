package org.example.framework.server.exception;

public class MarkNotSupportedException extends RuntimeException {
  public MarkNotSupportedException() {
  }

  public MarkNotSupportedException(String message) {
    super(message);
  }

  public MarkNotSupportedException(String message, Throwable cause) {
    super(message, cause);
  }

  public MarkNotSupportedException(Throwable cause) {
    super(cause);
  }

  public MarkNotSupportedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
