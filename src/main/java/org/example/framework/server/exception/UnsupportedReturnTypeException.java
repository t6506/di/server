package org.example.framework.server.exception;

public class UnsupportedReturnTypeException extends RuntimeException {
    public UnsupportedReturnTypeException() {
    }

    public UnsupportedReturnTypeException(final String message) {
        super(message);
    }

    public UnsupportedReturnTypeException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public UnsupportedReturnTypeException(final Throwable cause) {
        super(cause);
    }

    public UnsupportedReturnTypeException(final String message, final Throwable cause, final boolean enableSuppression,
                                          final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
