package org.example.framework.server.exception;

public class NoSupportingMessageConverter extends RuntimeException {
    public NoSupportingMessageConverter() {
    }

    public NoSupportingMessageConverter(final String message) {
        super(message);
    }

    public NoSupportingMessageConverter(final String message, final Throwable cause) {
        super(message, cause);
    }

    public NoSupportingMessageConverter(final Throwable cause) {
        super(cause);
    }

    public NoSupportingMessageConverter(final String message, final Throwable cause, final boolean enableSuppression,
                                        final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
