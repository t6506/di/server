package org.example.framework.server.exception;

public class UnsupportedParameterException extends RuntimeException {


    public UnsupportedParameterException() {
    }

    public UnsupportedParameterException(final String message) {
        super(message);
    }

    public UnsupportedParameterException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public UnsupportedParameterException(final Throwable cause) {
        super(cause);
    }

    public UnsupportedParameterException(final String message, final Throwable cause, final boolean enableSuppression,
                                         final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
